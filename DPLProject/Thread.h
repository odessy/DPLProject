#ifndef THREAD
#define THREAD

#ifdef WIN32
	#include <Windows.h>
#else
	#include <stdio.h>
	#include <pthread.h>     /* pthread functions and data structures */
#endif

/**
* Thread class based on
* https://www.relisoft.com/win32/active.html
*
*/

namespace Thread
{

#ifdef WIN32
	//Thread Class
	class Thread
	{
	protected:
			HANDLE _handle;
			DWORD _tid;

	public:
			Thread( DWORD (WINAPI * pFun) (void* arg), void* pArg)
				:_handle(CreateThread(
						NULL, // default security attributes
						0, //stack size
						pFun, // thread function
						pArg,  // argument to thread function
						CREATE_SUSPENDED,  // creation flags
						&_tid
						))
			{ }

			void Resume()
			{
				::ResumeThread(_handle);
			}

			void Suspend ()
			{
				::SuspendThread(_handle);
			}

			void WaitForDeath( unsigned timeoutMs = 10)
			{
				::WaitForSingleObject (_handle, timeoutMs);
			}

			bool IsAlive () const
			{
				unsigned long code;
				::GetExitCodeThread(_handle, &code);
				return code == STILL_ACTIVE;
			}
	};


	//Mutex Class

	class Mutex
	{
		friend class Lock;
	public:
		Mutex () { InitializeCriticalSection ( & _critSection); }
		~Mutex () { DeleteCriticalSection ( &_critSection); }
	private:
		void Acquire()
		{
			EnterCriticalSection( & _critSection);
		}

		void Release()
		{
			LeaveCriticalSection( & _critSection);
		}
		CRITICAL_SECTION _critSection;
	};

#else
	//Mutex Class

	class Mutex
	{
		friend class Lock;
	public:
		Mutex ():_critSection(PTHREAD_MUTEX_INITIALIZER) {}
		~Mutex () { pthread_mutex_destroy(&_critSection); }
	private:
		void Acquire()
		{
			if(pthread_mutex_lock( & _critSection))
			{
				perror("pthread_mutex_lock");
				pthread_exit(NULL);
			}
		}

		void Release()
		{
			if(pthread_mutex_unlock( & _critSection))
			{
				perror("pthread_mutex_unlock");
				pthread_exit(NULL);
			}
		}
		pthread_mutex_t _critSection;
	};


	class Thread
	{
	protected:
			pthread_t _handle;
			int _tid;
			pthread_cond_t got_request;
			pthread_mutex_t a_mutex;
	public:
		Thread( void * pFun, void * pArg)
			:_tid(pthread_create(&_handle, NULL, pFun, pArg )),
			got_request(PTHREAD_COND_INITIALIZER),
			a_mutex(PTHREAD_MUTEX_INITIALIZER)
		{
			Suspend();
		}

		~Thread()
		{
			if(pthread_cond_destroy(&got_request) == EBUSY)
			{
				//some thread is still waiting on this condition variable
				//handle case
			}
		}

		void Resume()
		{
			if(pthread_cond_signal(&got_request))
			{
				perror("pthread_cond_signal");
			}
		}

		void Suspend()
		{
			if(pthread_mutex_lock(&a_mutex))
			{
				perror("pthread_mutex_lock");
				pthread_exit(NULL);
			}

			pthread_cond_wait(&got_request, &a_mutex);
		}

		void WaitForDeath( unsigned timeoutMs = 10 )
		{
			pthread_cancel(_tid);
		}

		bool IsAlive() const
		{
		}

	};

#endif

	class Lock
	{
	public:
		//Aquire the state of the semaphore
		Lock ( Mutex & mutex )
			:_mutex(mutex)
		{
			_mutex.Acquire();
		}

		// Release the state of the semaphore
		~Lock ()
		{
			_mutex.Release();
		}

	private:
		Mutex & _mutex;

	};

	class ActiveObject
	{
	public:
		ActiveObject ();
		virtual ~ActiveObject () {}
		void Kill ();

	protected:
		virtual void InitThread () = 0;
		virtual void Loop () = 0;
		virtual void FlushThread () = 0;
		
#ifdef WIN32
		static DWORD WINAPI ThreadEntry (void *pArg);
#else
		static void ThreadEntry(void * pArg);
#endif

		int		_isDying;
		Thread	_thread;
	};

}

#endif