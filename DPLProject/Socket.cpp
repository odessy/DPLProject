#include "Socket.h"


//static reference
#ifdef WIN32
	WSAInitializer Socket::_Initializer_;
#endif

Socket::Socket(int socketfd)
{
	this->socketfd = socketfd;
}

Socket::Socket()
	:host(NULL), port(NULL), socketAddressInfo(NULL), closed(false), locked(false)
{
	initializeHints();
	socketfd = 0; //set socket to 0
}

Socket::Socket(const char * host, const char * port)
	:host(host), port(port), closed(false), locked(false)
{
	initializeHints();
	if(setAddrInfo())
	{
		createSocket();
	}
}

Socket::~Socket()
{
	if( locked == false ) closeSocket();
}

void Socket::initializeHints()
{
	memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC; // AF_INET or AF_INET6 to force version
    hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = 0;
	hints.ai_flags = AI_PASSIVE;
}

bool Socket::createSocket()
{
	if(socketAddressInfo != NULL)
	{
		if((socketfd = socket(socketAddressInfo->ai_family, socketAddressInfo->ai_socktype, 
			socketAddressInfo->ai_protocol)) < 0)
		{
			perror("Error : Could not create socket");
			return false;
		}
	}
	else
	{
		if( (socketfd = socket(AF_INET , SOCK_STREAM , 0)) == 0) 
		{
			perror("Error : Could not create socket");
			return false;
		}
	}

	return true;
}

bool Socket::listenSocket(int pending)
{
	if( listen(socketfd, pending) < 0)
	{
		perror("listen");
		return false;
	}

	return true;
}

int Socket::getAccept()
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof(their_addr);
	return accept(socketfd, (struct sockaddr *)&their_addr, &sin_size);
}

/** 
* Returns true on success, or false if there was an error 
*/
bool Socket::setSocketBlockingEnabled(bool blocking)
{
	if (this->socketfd < 0) return false;

#ifdef WIN32
   unsigned long mode = blocking ? 0 : 1;
   return (ioctlsocket(this->socketfd, FIONBIO, &mode) == 0) ? true : false;
#else
   int flags = fcntl(this->socketfd, F_GETFL, 0);
   if (flags < 0) return false;
   flags = blocking ? (flags&~O_NONBLOCK) : (flags|O_NONBLOCK);
   return (fcntl(this->socketfd, F_SETFL, flags) == 0) ? true : false;
#endif
}

bool Socket::setAddrInfo()
{
    int status;

    if ((status = getaddrinfo(host, port , &hints, &socketAddressInfo)) != 0) {
        std::cerr << "getaddrinfo: " << gai_strerror(status) << std::endl;
		return false;
    }
	return true;
}


int Socket::getSocketFd()
{
	return socketfd;
}

void Socket::setSocketFd(int socketfd)
{
	this->socketfd = socketfd;
}

struct addrinfo * Socket::getHints()
{
	return &hints;
}

struct addrinfo * Socket::getSocketAddressInfo()
{
	return socketAddressInfo;
}

/*
* print IPv4 and IPv6 address from addrinfo
*/
void Socket::showIp()
{
    struct addrinfo *p = socketAddressInfo;
    char ipstr[INET6_ADDRSTRLEN];

    if(p != NULL) {
        void *addr = getInAddress(p);
        // convert the IP to a string and print it:
        inet_ntop(p->ai_family, addr, ipstr, sizeof ipstr);
        std::cout << getAiFamily(p->ai_family).c_str() <<" : "<< ipstr << std::endl;
    }
}

/*
* return ip version based on ai_family
*/
std::string Socket::getAiFamily(int family)
{
	std::string ipVersion = "IPv6";

    if (family == AF_INET) { // IPv4
        ipVersion = "IPv4";
    } 
    return ipVersion;
}

/* 
* get the pointer to the address itself,
* different fields in IPv4 and IPv6:
*/
void * Socket::getInAddress(struct sockaddr * socketAddress)
{
    if (socketAddress->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)&socketAddress)->sin_addr);
    }

    return &(((struct sockaddr_in6*)&socketAddress)->sin6_addr);
}

void * Socket::getInAddress(struct addrinfo * addressInfo)
{
	return getInAddress(addressInfo->ai_addr);
}

void * Socket::getInAddress()
{
	return getInAddress(&socketAddress);
}

void Socket::sigchld_handler(int s)
{
	#ifndef WIN32
    while(waitpid(-1, NULL, WNOHANG) > 0);
	#endif
}

void Socket::closeSocket()
{
	if( socketfd > 0 )
	{
		#ifdef WIN32
			closesocket( socketfd );
		#else
			close( socketfd );
		#endif
		//set socket alive to false
		setClosed(true);
		setSocketFd(0);
	}
}

bool Socket::isClosed()
{
	return closed;
}

void Socket::setClosed(bool val)
{
	closed = val;
}

int Socket::setSendTimeout(int sec, int usec)
{
	struct timeval tmo = {0};
    tmo.tv_sec = sec;
    tmo.tv_usec = usec;

    if (-1 == setsockopt(socketfd, SOL_SOCKET, SO_SNDTIMEO, (char *)&tmo, sizeof(tmo))) {
		 perror("setsockopt");
		 return false;
    }
	return true;
}

int Socket::setReceiveTimeout(int sec, int usec)
{
	struct timeval tmo = {0};
    tmo.tv_sec = sec;
    tmo.tv_usec = usec;

    if (-1 == setsockopt(socketfd, SOL_SOCKET, SO_RCVTIMEO, (char *)&tmo, sizeof(tmo))) {
		 perror("setsockopt");
		 return false;
    }
	return true;
}

bool Socket::multipleConnection(bool enable)
{
	char yes = '1';

	if(enable == false) yes = '0';

    if (setsockopt( socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
        perror("setsockopt");
        return false;
    }

	return true;
}

bool Socket::bindAddress()
{
    if (bind( socketfd, socketAddressInfo->ai_addr, socketAddressInfo->ai_addrlen) == -1)
	{
		closeSocket();
        perror("server: bindAddress failed");
        return false;
    }
	return true;
}

bool Socket::connectTo(int socketfd, struct sockaddr * addr, int size )
{
	if(connect(socketfd, addr, size) == -1)
	{
		//perror("connect");
		return false;
	}

	return true;
}

Socket * Socket::connectToRemote(const char * host, const char * port)
{
	Socket * remote = new Socket();

	struct hostent *hp;
	struct sockaddr_in addr;
	int on = 1, socketfd; 

	if((hp = gethostbyname(host)) == NULL){
		perror("gethostbyname");
		return NULL;
	}

	std::memcpy(&addr.sin_addr, hp->h_addr, hp->h_length);
	addr.sin_port = htons(std::atoi(port));
	addr.sin_family = AF_INET;

	socketfd = socket(PF_INET, SOCK_STREAM, 0);

	if(socketfd == -1){
		perror("setsockopt");
		return NULL;
	}

	if(!connectTo(socketfd, (struct sockaddr *)&addr, sizeof(struct sockaddr_in))){
		return NULL;
	}

	remote->setSocketFd(socketfd);

	return remote;
}

bool Socket::sendAll(void * buffer, size_t length, int flags, int retry)
{
	char * ptr = (char *) buffer;
	int slen = length;
	int times = retry;

	while(slen > 0)
	{
		//printf("\nsendAll() Looping\n");
		int sent_length = send(socketfd, ptr, slen, flags);
		if( sent_length  > 0)
		{
			ptr += sent_length;
			slen = slen - sent_length;
		}
		else
		{
			if( times > 0)
			{
				times --;
			}
		}

		if( slen > 0 && times == 0 ) return false;
	}

	return true;
}

void Socket::stream(Socket * otherfd)
{
	if(this->socketfd == otherfd->socketfd) return;

	size_t   dataRead;

	char buffer[BUFFER_SIZE];

	dataRead = read(&buffer[0], BUFFER_SIZE, 0);
	printf("> Recieved %d\n", dataRead);
	otherfd->sendAll(buffer, dataRead, 0);
}

int Socket::read(char * buffer, size_t  size, size_t offset)
{
	return recv(socketfd, &buffer[offset], size-1, 0);
}

int Socket::hostname_to_ip( const char * hostname, char * ip)
{
    int sockfd;  
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_in *h;
    int rv;
 
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC; // use AF_INET6 to force IPv6
    hints.ai_socktype = SOCK_STREAM;
 
    if ( (rv = getaddrinfo( hostname , "http" , &hints , &servinfo)) != 0) 
    {
        printf("getaddrinfo: %s\n", gai_strerror(rv));
        return 0;
    }
 
    //loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) 
    {
        h = (struct sockaddr_in *) p->ai_addr;
        strcpy(ip , inet_ntoa( h->sin_addr ) );
    }
     
    freeaddrinfo(servinfo); // all done with this structure
    return 1;
}


