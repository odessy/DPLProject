#ifndef SYNTAX_H
#define SYSNTAX_H

#include "SymbolTable.h"
#include "SymbolReference.h"
#include "ExceptionHandler.h"


class Syntax
{

public:
	//check and validates syntax of song file
	static void checkPuntuations(SymbolTablePtr);
	static void checkStart(SymbolTablePtr);
	static void checkStop(SymbolTablePtr);
	static void checkCurveBracket(SymbolTablePtr);
	static void checkQBracket(SymbolTablePtr);
	static void checkCrlyBracket(SymbolTablePtr);
	static void checkBracket(SymbolTablePtr symbolTable, char open, char close, const CHARPTR errorMessage = "Missing Bracket");

};
#endif