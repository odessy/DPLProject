#ifndef MULTIPART_CONSUMER
#define MULTIPART_CONSUMER

#include "multipart_parser.h"
#include "GenInclude.h"
#include <map>

class MultipartConsumer
{
public:

	map<string, const char *> headers; 

    MultipartConsumer(const std::string& boundary)
		:m_headers(0)
    {
        memset(&m_callbacks, 0, sizeof(multipart_parser_settings));
        //m_callbacks.on_header_field = ReadHeaderName;
        m_callbacks.on_header_value = ReadHeaderValue;
		m_callbacks.on_part_data = ReadPartData;
		m_callbacks.on_part_data_end = ReadPartDataEnd;

        m_parser = multipart_parser_init(boundary.c_str(), &m_callbacks);
        multipart_parser_set_data(m_parser, this);
    }

    ~MultipartConsumer()
    {
        multipart_parser_free(m_parser);
    }

    int CountHeaders(const std::string& body)
    {
        multipart_parser_execute(m_parser, body.c_str(), body.size());
        return m_headers;
    }

private:
	//static int ReadHeaderName(multipart_parser* p, const char *at, size_t length)
	//{
	//	MultipartConsumer * me = (MultipartConsumer*)multipart_parser_get_data(p);
	//	me->m_headers++;
	//	printf("%.*s: ", length, at);
	//	return 0;
	//}

	static int ReadHeaderValue(multipart_parser* p, const char *at, size_t length)
	{
		MultipartConsumer * me = (MultipartConsumer*)multipart_parser_get_data(p);
		//printf("%.*s\n", length, at);
		if(length > 8 && at[0] == 'f' && at[8] == 'a')
			me->setHeaderKey(at, length);
		me->m_headers++;
		return 0;
	}

	static int ReadPartData(multipart_parser* p, const char *at, size_t length)
    {
		MultipartConsumer * me = (MultipartConsumer*)multipart_parser_get_data(p);
		me->setHeaderData(at, length);
		//printf("%.*s\n", length, at);
		return 0;
    }

	static int ReadPartDataEnd(multipart_parser* p)
    {
		MultipartConsumer * me = (MultipartConsumer*)multipart_parser_get_data(p);

		return 0;
    }

	void setHeaderKey(const char * at, size_t length)
	{
		string skey = string(at, length);
		int loc = skey.find('=');
		skey = skey.substr(loc+2, skey.length());
		int end = (skey.find(';') != -1) ? skey.find(';') : skey.length();
		skey = skey.substr(0, end-1);
		key = skey;
	}

	void setHeaderData(const char * at, size_t length)
	{
		if(!key.empty())
		{
			char * buf = new char[length+1];
			memset(buf, '\0', length+1);
			memcpy(buf, at, length);
			headers[key] = buf;
		}
		key = "";
	}

    multipart_parser* m_parser;
    multipart_parser_settings m_callbacks;
    int m_headers;
	string key;
};

#endif