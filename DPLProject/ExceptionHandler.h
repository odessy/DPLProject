#ifndef EXCEPTION_HANDLER_H
#define EXCEPTION_HANDLER_H

#include <string>
#include <iostream>
#include <exception>
#include <stdexcept>
#include <sstream>
#include "SymbolTable.h"
#include "speak\translate.h"

using namespace std;

class ExceptionHandler : public runtime_error
{
public:
	ExceptionHandler(const char * message)
		:runtime_error(message)
	{
	}

	ExceptionHandler(const char * message , SymbolReferencePtr & ref)
		:runtime_error(getMessage(message , ref))
	{
	}

	ExceptionHandler(const char * message , SymbolReferenceGroupPtr & ref)
		:runtime_error(getMessage(message , ref))
	{
	}

	const char * _getMessage(stringstream & stream)
	{
		int len = stream.str().size();
		char * fullMessage = new char[len];
		
		char x;
		int i = 0;
		while( ( x = stream.get() ) != EOF)
		{
			if(i == len-1) break;
			utf8_out((int)x, &fullMessage[i]);
			i++;
		}

		fullMessage[len-1] = '\0';
		return fullMessage;
	}

	const char * getMessage(const char * message , SymbolReferencePtr & ref)
	{
		stringstream stream;
		stream << message;
		stream << " [ ";
		stream << ref->getValue();
		stream << " LOCATION { LineNumber: " + to_string(ref->getLineNumber())
			+ ", ColumnNumber: " + to_string(ref->getColumnNumber()) + " } Type: " + ref->getStringType() + " ]";

		return _getMessage(stream);
	}

	const char * getMessage(const char * message , SymbolReferenceGroupPtr & ref)
	{
		stringstream stream;
		stream << message;
		stream << " [ ";
		stream << getGroupMessage(ref);
		stream << " LOCATION { LineNumber: " + to_string(ref->at(0)->getLineNumber())
			+ ", ColumnNumber: " + to_string(ref->at(0)->getColumnNumber()) + " } Type: " + ref->at(0)->getStringType() + " ]";

		return _getMessage(stream);
	}

	const char * getGroupMessage(SymbolReferenceGroupPtr & ref)
	{
		int i = 0;
		CHARPTR refGroup = new char[ref->size() + 1];
		for(i = 0; i < ref->size(); i++)
		{
			refGroup[i] = ref->at(i)->getValue();
		}
		refGroup[i] = '\0';
		return refGroup;
	}

	virtual const char* what() const throw()
	{
		return runtime_error::what();
	}
};

#endif