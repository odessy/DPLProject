#ifndef SEMANTIC_H
#define SEMANTIC_H

#include "GenInclude.h"
#include "SymbolTable.h"
#include "SymbolReference.h"
#include "ExceptionHandler.h"

class semantic {

public:
	static void checkSemantics(SymbolTablePtr sTable);
	static void checkLabel(SymbolTablePtr sTable, char open, char close, static bool callBack(SymbolReferenceGroupPtr), const CHARPTR errorMessage = "Semantic error Incorrect Label at " );

	static void checkAll(SymbolTablePtr);
	//static void nestingBracesCheck(SymbolTablePtr);	

	static bool checkAllLabels(SymbolReferenceGroupPtr);
	static bool checkVerseCharacters(SymbolReferenceGroupPtr);
	static bool checkChorusCharacters(SymbolReferenceGroupPtr);

	static bool matchLabelGroup(SymbolReferenceGroupPtr ptrA,  SymbolReferenceGroupPtr ptrB);
	static void checkForSongContent(SymbolTablePtr sTable, char open, char close);

	static void checkForSongContent(SymbolTablePtr);
	static void checkWordsBetweenBrackets(SymbolTablePtr);
	static void checkWordsBetweenCurlyBrackets(SymbolTablePtr);

};
#endif