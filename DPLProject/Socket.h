#ifndef SOCKET_H
#define SOCKET_H

#include "socket_include.h"
#include <iostream>
#include <signal.h>


#define BUFFER_SIZE 1024 * 8 //8 kbyte

class Socket{

private:
	#ifdef WIN32
		static WSAInitializer _Initializer_;
	#endif

	struct addrinfo hints, * socketAddressInfo;
	int socketfd;
	const char * port, * host;
	struct sockaddr socketAddress;
	bool closed;
	bool locked;

public:
	Socket();
	Socket(int socketfd);
	Socket(const char * host, const char * port);
	~Socket();
	void initializeHints();
	bool createSocket();
	void closeSocket();
	bool isClosed();
	void setClosed(bool val);
	bool listenSocket(int pending);
	int getAccept();
	static bool connectTo(int socketfd, struct sockaddr * addr, int size );
	static Socket * connectToRemote(const char * host, const char * port);
	bool setSocketBlockingEnabled(bool blocking);
	struct addrinfo * getSocketAddressInfo();
	void showIp();
	bool setAddrInfo();
	void * getInAddress();
	static void * getInAddress(struct sockaddr *);
	static void * getInAddress(struct addrinfo *);
	static std::string Socket::getAiFamily(int);
	static int hostname_to_ip( const char * hostname, char * ip);
	void sigchld_handler(int);
	bool multipleConnection(bool enable = true);
	int setSendTimeout(int sec, int usec);
	int setReceiveTimeout(int sec, int usec);
	bool bindAddress();
	int getSocketFd(); //returns socketfd
	void setSocketFd(int socketfd);
	struct addrinfo * getHints();
	bool sendAll(void *, size_t, int, int = 10);
	bool sendData(void *, size_t);
	void stream(Socket * otherfd);
	int read(char * buffer, size_t size, size_t offset);

	void setLocked(bool locked){ this->locked = locked; }
	bool isLocked(){ return this->locked; }
};

#endif