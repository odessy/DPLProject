#ifndef LEXICAL_H
#define LEXICAL_H

#include "GenInclude.h"
#include "SymbolTable.h"
#include <sstream>

#define LexicalPtr Lexical *

class Lexical {

public:
	Lexical(string fileName);
	Lexical::Lexical();
	void readBuffer(const char *);
	void readFile();
	bool isPuntuation(char element);
	bool isWord(char element);
	bool isLanguage(char element);
	size_t refSum(); //returns elementCount * sizeof(char)
	EnumElementType getElementType(char element);
	void processElement(char element);
	SymbolTable symbolTable;

private:
	string fileName;
	int lineNumber;
	int elementCount;
	int columnCount;
	int groupNumber;
	char prevElement;
};

#endif