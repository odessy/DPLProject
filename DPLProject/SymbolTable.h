#ifndef SYMBOL_TABLE
#define SYMBOL_TABLE

#include "GenInclude.h"
#include "SymbolReference.h"

#include <vector>
#include <queue>
#include <map>

#define SymbolReferenceGroup vector<SymbolReferencePtr>
#define SymbolReferenceGroupPtr vector<SymbolReferencePtr> *
#define SymbolTableVector vector<SymbolReferenceGroupPtr>

typedef struct MapValue{
	CHARPTR charPtr;
	SymbolReferenceGroupPtr refs;
}MapValue;

#define ValueMap map<char, MapValue>
#define SymbolTablePtr SymbolTable *

class SymbolTable{
public:
	SymbolTable();
	~SymbolTable();
	void addSymbolReference(char value, EnumElementType type, 
		int lineNumber, int columnNumber, int groupNumber, size_t seekRef);
	void addToReferenceGroup(SymbolReferencePtr);
	SymbolReferenceGroupPtr getSymbolReferenceGroup(int index);
	int getSize();
	void displayValueMap();
	void displaySymbolTable();
	const MapValue getMapValue(char ch);
private:
	SymbolTableVector symbolTableVector;
	ValueMap valueMap;
};

#endif