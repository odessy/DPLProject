#include "Thread.h"

Thread::ActiveObject::ActiveObject ()
	:_isDying (0),
//#pragma warning(disable: 4355) // 'this' used before initialized
	_thread (ThreadEntry, this)
//#pragma warning (default: 4355)
{

}

void Thread::ActiveObject::Kill()
{
	_isDying++;
	FlushThread();
	// Let's make sure it's gone
	_thread.WaitForDeath();
}


#ifdef WIN32
	DWORD WINAPI Thread::ActiveObject::ThreadEntry (void* pArg)
	{
		ActiveObject * pActive = (ActiveObject *) pArg;
		pActive->InitThread();
		pActive->Loop();
		return 0;
	}
#else
	void Thread::ActiveObject::ThreadEntry (void* pArg)
	{
		ActiveObject * pActive = (ActiveObject *) pArg;
		pActive->InitThread();
		pActive->Loop();
	}
#endif