#ifndef SOCKET_INCLUDE_H
#define SOCKET_INCLUDE_H

#include "GenInclude.h"

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <winsock2.h>	// For socket(), connect(), send(), and recv()
#include <ws2tcpip.h> // For INET6_ADDRSTRLEN ,  getaddrinfo(), freeaddrinfo(), inet_ntop(), gai_strerror()

// Need to link with Ws2_32.lib
#pragma comment (lib, "Ws2_32.lib")

#define STR_TO_DEC( x ) dynamic_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()

class WSAInitializer
{
	private:
		WSADATA wsaData;   // if this doesn't work

	public:
	  WSAInitializer(){
			// MAKEWORD(1,1) for Winsock 1.1, MAKEWORD(2,0) for Winsock 2.0:

			if (WSAStartup(MAKEWORD(1,1), &wsaData) != 0) {
				fprintf(stderr, "WSAStartup failed.\n");
				exit(1);
			}
	}

	~WSAInitializer() {
			WSACleanup();
	}

};

#endif