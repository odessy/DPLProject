#include "Server.h"


Server::Server(int maxClients)
	:maxClients(maxClients)
{
	initialize();

	//allow multiple connection to Server
	if( !socket->multipleConnection())
		exit(1);

	//bind socket to port
	if( !socket->bindAddress() )
	{  
		//TODO change to APP Class
		cout << "Failed to bind to Address";
		exit(1);
	}

	if( !socket->listenSocket(BACKLOG) ) 
	{
		cout << "Failed to listen";
        exit(1);
    }

	_thread.Resume();
}

Server::~Server()
{ 
	socket->closeSocket(); 
	Kill(); 
}

void Server::initialize()
{
	//other initialization

	//client_socket = new Socket*[maxClients];

	//for(int i = 0; i < maxClients; i++) 
	//	client_socket[i] = new Socket();

	socket = new Socket(SERVER_HOST, SERVER_PORT);
}


void Server::Loop()
{
	printf("server: waiting for connections on %s ...\n", SERVER_PORT);

	int mainfd = socket->getSocketFd();

	FD_ZERO(&activeFdSet);

	// main accept() loop
    while(1) 
	{   
		//initialize set of active connections

		maxSd = socket->getSocketFd();

		int new_fd = socket->getAccept();
		if (new_fd == -1) {
			perror("accept");
			continue;
		}
		else
		{
			//printf("%d\n", new_fd);
			bool set = false;

			Socket * socket = new Socket(new_fd);
			RequestHandler::active ++;
			new RequestHandler(socket);
		}

		//Sleep(100);
	}
}