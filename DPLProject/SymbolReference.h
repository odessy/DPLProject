#ifndef DPL_SYMBOL_REFERENCE
#define DPL_SYMBOL_REFERENCE

#include "GenInclude.h"

////Definition of puntuations
#define START '@'
#define STOP '#'
#define COMMA ','
#define OPENBRACKET '('
#define CLOSEBRACKET ')'
#define APOS '\''
#define NEWLINE '\n'
#define LINEFEED '\r'
#define TAB '\t'
#define SPACE ' '
#define PERIOD '.'
#define OPENSQBRACKET '['
#define CLOSESQBRACKET ']'
#define OPENCRLYBRACKET '{'
#define CLOSECRLYBRACKET '}'
#define BACKSLASH '/'
#define FORWARDSLASH '\\'
#define NULLCHAR '\0'
#define ASTARICK '*'


//Definition of puntuation names
#define DPL_WORD "WORD"
#define START_NAME "START"
#define STOP_NAME "STOP"
#define COMMA_NAME "COMMA"
#define OPENBRACKET_NAME "OPENBRACKET"
#define CLOSEBRACKET_NAME "CLOSEBRAKCET"
#define APOS_NAME "APOS"
#define NEWLINE_NAME "NEWLINE"
#define NEWLINE_SHORT_NAME "NL"
#define TAB_NAME "TAB"
#define SPACE_NAME "SPACE"
#define PERIOD_NAME "PERIOD"
#define OPENSQLBRACKET_NAME "OPENSQBRACKET"
#define CLOSESQLBRACKET_NAME "CLOSESQBRACKET"
#define OPENCRLYBRACKET_NAME "OPENCRLYBRACKET"
#define CLOSECRLYBRACKET_NAME "CLOSECRLYBRACKET"
#define BACKSLASH_NAME "BACKSLASH"
#define FORWARDSLASH_NAME "FORWARDSLASH"

#define CHARPTR char *

#define SymbolReferencePtr SymbolReference *

enum ElementType{ _WORD, _PUNTUATION, _LANGUAGE, _UNKNOWN  };

#define EnumElementType enum ElementType

const char puntuations[] = { COMMA, OPENBRACKET, CLOSEBRACKET, 
							NEWLINE, APOS, TAB, SPACE, PERIOD,
							BACKSLASH, FORWARDSLASH,
							OPENSQBRACKET, CLOSESQBRACKET,
							OPENCRLYBRACKET, CLOSECRLYBRACKET };

const char braces[] = { OPENBRACKET, CLOSEBRACKET, 
						OPENSQBRACKET, CLOSESQBRACKET,
						OPENCRLYBRACKET, CLOSECRLYBRACKET };

/*
* SymbolReference class used to store reference to a symbol table
*/
class SymbolReference{
public:
	SymbolReference(CHARPTR value, EnumElementType type, 
		int lineNumber, int columnNumber, int groupNumber, size_t seekRef);
	void display();
	//getters
	char getValue();
	EnumElementType getType();
	int getLineNumber();
	int getColumnNumber();
	int getGroupNumber();
	size_t getSeekRef();
	string getStringType();
	void setType(EnumElementType type);
private:
	CHARPTR value;
	EnumElementType type;
	int lineNumber;
	int columnNumber;
	int groupNumber;
	size_t seekRef;
};

#endif