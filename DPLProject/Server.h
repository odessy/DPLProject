#ifndef SERVER_H
#define SERVER_H

#include "Socket.h" //place before thread since namespace Thread
#include "Thread.h"
#include "Request.h"

#define BACKLOG 10     // how many pending connections queue will hold

#define SERVER_HOST "0.0.0.0"
#define SERVER_PORT "1020"

class Server : public Thread::ActiveObject
{
public:
	void Start(){ _thread.Resume(); }
	void Stop(){ _thread.Suspend(); }
	Server(int maxClients);
	~Server();

private:
	void initialize();
	void InitThread () {}
	void Loop();
	void FlushThread() {}

	Socket ** client_socket, * socket;
	int maxSd, addrlen, valread, i, maxClients;
	fd_set activeFdSet;
};

class ServerHandler : public Thread::ActiveObject
{

public:
	void Start(){ _thread.Resume(); }
	ServerHandler(int maxClients, fd_set * activeFdSet)
		:maxClients(maxClients), activeFdSet(activeFdSet)
	{ };
	~ServerHandler(){ Kill(); }

private:
	void InitThread () {}
	void Loop();
	void FlushThread() {}

	fd_set * activeFdSet;
	int maxClients;
};

#endif