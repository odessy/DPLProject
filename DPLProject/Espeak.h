#ifndef DPL_ESPEAK_H
#define DPL_ESPEAK_H

#include "GenInclude.h"
#include "speak\speak_lib.h"
#include "speak\synthesize.h"

extern int samplerate;
extern int quiet;
extern unsigned int samples_total;
extern unsigned int samples_split;
extern unsigned int samples_split_seconds;
extern unsigned int wavefile_count;
extern bool allowMusic;

extern FILE *f_wavfile;
extern char filetype[5];
extern char wavefile[200];

void Write4Bytes(FILE *f, int value);
int OpenWavFile(char *path, int rate);
void CloseWavFile();
int SynthCallback(short *wav, int numsamples, espeak_EVENT *events);
int start (const char *buffer, const char * voicename, const char * fileName, int option_waveout);
int openWav(string fileName, double*& left, double*& right);

#endif