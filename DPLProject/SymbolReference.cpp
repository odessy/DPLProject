#include "SymbolReference.h"

SymbolReference::SymbolReference(CHARPTR value, EnumElementType type, int lineNumber, int columnNumber, int groupNumber, size_t seekRef)
	:value(value), type(type), lineNumber(lineNumber), columnNumber(columnNumber), groupNumber(groupNumber), seekRef(seekRef)
{

}

char SymbolReference::getValue()
{
	return *value;
}

EnumElementType SymbolReference::getType()
{
	return type;
}

int SymbolReference::getLineNumber()
{
	return lineNumber;
}

int SymbolReference::getColumnNumber()
{
	return columnNumber;
}

int SymbolReference::getGroupNumber()
{
	return groupNumber;
}

size_t SymbolReference::getSeekRef()
{
	return seekRef;
}

void SymbolReference::setType(EnumElementType type)
{
	this->type = type;
}

void SymbolReference::display()
{
	string x = " ";
	x[0] = *value;

	if( *value == NEWLINE ) x = NEWLINE_SHORT_NAME; 

	cout << x << SPACE
		<< OPENBRACKET << lineNumber << COMMA 
		<< columnNumber << CLOSEBRACKET 
		<< SPACE << getStringType()
		<< " Seek_Reference: " << seekRef
		<< " Group_Number: " << groupNumber
		<< endl;
}

string SymbolReference::getStringType()
{
	switch (type)
	{
	case _WORD:
		return DPL_WORD;
		break;
	case _PUNTUATION:
		return "PUNTUATION";
		break;
	case _LANGUAGE:
		return "LANGUAGE";
		break;
	default:
		return "UNKNOWN";
		break;
	}

}