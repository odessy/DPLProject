#include "Espeak.h"
#include "Lexical.h"
#include "ExceptionHandler.h"
#include "Syntax.h"
#include "Server.h"
#include "semantic.h"

int samplerate = 44100;
int quiet = 0;
unsigned int samples_total = 0;
unsigned int samples_split = 0;
unsigned int samples_split_seconds = 0;
unsigned int wavefile_count = 0;
bool allowMusic = false;

FILE *f_wavfile = NULL;
char filetype[5];
char wavefile[200];


static double * music = NULL;
static double * rightM = NULL;
static int size = 0;
static int index = 0;


void Write4Bytes(FILE *f, int value)
{//=================================
// Write 4 bytes to a file, least significant first
	int ix;

	for(ix=0; ix<4; ix++)
	{
		fputc(value & 0xff,f);
		value = value >> 8;
	}
}

int OpenWavFile(char *path, int rate)
//===================================
{
	static unsigned char wave_hdr[44] = {
		'R','I','F','F',0x24,0xf0,0xff,0x7f,'W','A','V','E','f','m','t',' ',
		0x10,0,0,0,1,0,1,0,  9,0x3d,0,0,0x12,0x7a,0,0,
		2,0,0x10,0,'d','a','t','a',  0x00,0xf0,0xff,0x7f};

	if(path == NULL)
		return(2);

	while(isspace(*path)) path++;

	f_wavfile = NULL;
	if(path[0] != 0)
	{
		if(strcmp(path,"stdout")==0)
			f_wavfile = stdout;
		else
			f_wavfile = fopen(path,"wb");
	}
	
	if(f_wavfile == NULL)
	{
		fprintf(stderr,"Can't write to: '%s'\n",path);
		return(1);
	}

	fwrite(wave_hdr,1,24,f_wavfile);
	Write4Bytes(f_wavfile,rate);
	Write4Bytes(f_wavfile,rate * 2);
	fwrite(&wave_hdr[32],1,12,f_wavfile);
	return(0);
}   //  end of OpenWavFile

void CloseWavFile()
//========================
{
	unsigned int pos;

	if((f_wavfile==NULL) || (f_wavfile == stdout))
		return;

	fflush(f_wavfile);
	pos = ftell(f_wavfile);

	fseek(f_wavfile,4,SEEK_SET);
	Write4Bytes(f_wavfile,pos - 8);

	fseek(f_wavfile,40,SEEK_SET);
	Write4Bytes(f_wavfile,pos - 44);

	fclose(f_wavfile);
	f_wavfile = NULL;

} // end of CloseWavFile

int start (const char *buffer, const char * voicename, const char * fileName, int option_waveout)
{
	static const char* err_load = "Failed to read ";

	FILE *f_text=NULL;
	char *p_text=NULL;
	FILE *f_phonemes_out = stdout;
	char *data_path = NULL;   // use default path for espeak-data

	int option_index = 0;
	int c;
	int ix;
	int value;
	int flag_stdin = 0;
	int flag_compile = 0;
	int filesize = 0;
	int synth_flags = espeakCHARS_AUTO | espeakPHONEMES | espeakENDPAUSE;

	int volume = -1;
	int speed = -1;
	int pitch = -1;
	int wordgap = -1;
	int option_capitals = -1;
	int option_punctuation = -1;
	int phonemes_separator = 0;
	int phoneme_options = 0;
	int option_linelength = 0;

	espeak_VOICE voice_select;
	
	#define N_PUNCTLIST  100

	wchar_t option_punctlist[N_PUNCTLIST];

	sprintf( wavefile, "%s", fileName);

	option_punctlist[0] = 0;

	phoneme_options |= espeakPHONEMES_SHOW;

	if(option_waveout || quiet)
	{
		// writing to a file (or no output), we can use synchronous mode
		samplerate = espeak_Initialize(AUDIO_OUTPUT_SYNCHRONOUS,0,data_path,0);
		samples_split = samplerate * samples_split_seconds;

		espeak_SetSynthCallback(SynthCallback);
		if(samples_split)
		{
			char *extn;
			extn = strrchr(wavefile,'.');
			if((extn != NULL) && ((wavefile + strlen(wavefile) - extn) <= 4))
			{
				strcpy(filetype,extn);
				*extn = 0;
			}
		}
	}
	else
	{
		// play the sound output
		samplerate = espeak_Initialize(AUDIO_OUTPUT_SYNCH_PLAYBACK,0, data_path,0);
	}

	if(espeak_SetVoiceByName(voicename) != EE_OK)
	{
		memset(&voice_select,0,sizeof(voice_select));
		voice_select.languages = voicename;
		if(espeak_SetVoiceByProperties(&voice_select) != EE_OK)
		{
			fprintf(stderr,"%svoice '%s'\n",err_load,voicename);
			exit(2);
		}
	}

	// set any non-default values of parameters. This must be done after espeak_Initialize()
	if(speed > 0)
		espeak_SetParameter(espeakRATE,speed,0);
	if(volume >= 0)
		espeak_SetParameter(espeakVOLUME,volume,0);
	if(pitch >= 0)
		espeak_SetParameter(espeakPITCH,pitch,0);
	if(option_capitals >= 0)
		espeak_SetParameter(espeakCAPITALS,option_capitals,0);
	if(option_punctuation >= 0)
		espeak_SetParameter(espeakPUNCTUATION,option_punctuation,0);
	if(wordgap >= 0)
		espeak_SetParameter(espeakWORDGAP,wordgap,0);
	if(option_linelength > 0)
		espeak_SetParameter(espeakLINELENGTH,option_linelength,0);
	if(option_punctuation == 2)
		espeak_SetPunctuationList(option_punctlist);
	

	espeak_SetPhonemeTrace(phoneme_options | (phonemes_separator << 8), f_phonemes_out);

	Lexical * lex = new Lexical();
	lex->readBuffer(buffer);

	//lex->symbolTable.displaySymbolTable();
	
	//Exception willl be throw here
	Syntax::checkPuntuations(&lex->symbolTable);
	semantic::checkSemantics(&lex->symbolTable);
	
	string tokens = "";
	string token = "";
	int e = 0;

	for(;;)
	{
		if( e > lex->symbolTable.getSize() ) break;

		SymbolReferenceGroupPtr group = lex->symbolTable.getSymbolReferenceGroup(e);

		if( group != NULL )
		{
			if( group->at(0)->getType() == ElementType::_WORD ){

				for(int i = 0; i < group->size(); i++)
				{
					token += group->at(i)->getValue();
				}

				token += " ";
			}
			else 
			{
				speed = 140 + rand() % 100;
				volume = 50 + rand() % 50; 
				espeak_SetParameter(espeakRATE,speed,0);
				espeak_SetParameter(espeakVOLUME,volume,0);

				tokens += token;
				//if( !token.empty() && lex->isPuntuation(group->at(0)->getValue()) )
				//	tokens += NEWLINE;
				if(!token.empty()) 
				{
					espeak_Synth(token.c_str(),token.size(),0,POS_SENTENCE,0,synth_flags,NULL,NULL);
					allowMusic = false;
				}
				else
					allowMusic = true;
				token = "";
			}
		}

		if(espeak_Synchronize() == EE_OK)
			e++;
	}

	CloseWavFile();
}

int SynthCallback(short *wav, int numsamples, espeak_EVENT *events)
{//========================================================================
	char fname[210];

	if(quiet) return(0);  // -q quiet mode

	if(wav == NULL)
	{
		//CloseWavFile();
		return(0);
	}

	while(events->type != 0)
	{
		if(events->type == espeakEVENT_SAMPLERATE)
		{
			samplerate = events->id.number;
			samples_split = samples_split_seconds * samplerate;
		}
		else
		if(events->type == espeakEVENT_SENTENCE)
		{
			// start a new WAV file when the limit is reached, at this sentence boundary
			if((samples_split > 0) && (samples_total > samples_split))
			{
				CloseWavFile();
				samples_total = 0;
				wavefile_count++;
			}
		}
		events++;
	}

	if(f_wavfile == NULL)
	{
		if(samples_split > 0)
		{
			sprintf(fname,"%s_%.2d%s",wavefile,wavefile_count+1,filetype);
			if(OpenWavFile(fname, samplerate) != 0)
				return(1);
		}
		else
		{
			if(OpenWavFile(wavefile, samplerate) != 0)
				return(1);
		}
	}

	if(numsamples > 0)
	{
		short * outputwave = new short[numsamples];
		memset(outputwave, 0, numsamples);

		for(int i = 0; i < numsamples;)
		{
			//outputwave[i] = wav[i];
			//outputwave[i+1] = wav[i+1];

			if(index == size)
			{
				size = openWav("./samples/test.wav", music, rightM);
				index = 0;
			}

			if(index < size)
			{
				//mix wav here before write to the fill

				if (music[index] < -1.0) music[index] = -1.0;
				if (music[index] > +1.0) music[index] = +1.0;

				short s = (short) 32767 * music[index];

				//if ( wav[i] < s){

					samples_total += numsamples;

					//if(!allowMusic)
					//{
						outputwave[i] = wav[i] + ( (s) * .4 );// | 1;
						//outputwave[i+1] = ( s >> 8 );
					//}
				//}
				
				index++;
			}

			i++;
		}

		fwrite(outputwave,numsamples*2,1,f_wavfile);
	}
	return(0);
}

static double bytesToDouble(char firstByte, char secondByte) {
    // convert two bytes to one short (little endian)
	return ((short) (((secondByte & 0xFF) << 8) + (firstByte & 0xFF))) / ((double) 32767.0);
}

// Returns left and right double arrays. 'right' will be null if sound is mono.
int openWav(string fileName, double *& left, double *& right)
{
	ifstream inFile(fileName, ios::binary);

	if (!inFile.is_open()) {
		cout << "Unable to open file: " << fileName << endl;
        return 0; 
	}

	//read wave file into array
    inFile.seekg (0, inFile.end);
    int size = inFile.tellg();
    inFile.seekg (0, inFile.beg);
    char * wav = new char [size];
    inFile.read (wav,size);


    // Determine if mono or stereo
    int channels = wav[22];     // Forget byte 23 as 99.999% of WAVs are 1 or 2 channels

    // Get past all the other sub chunks to get to the data subchunk:
    int pos = 12;   // First Subchunk ID from 12 to 16

    // Keep iterating until we find the data chunk (i.e. 64 61 74 61 ...... (i.e. 100 97 116 97 in decimal))
    while(!(wav[pos]==100 && wav[pos+1]==97 && wav[pos+2]==116 && wav[pos+3]==97)) {
        pos += 4;
        int chunkSize = wav[pos] + wav[pos + 1] * 256 + wav[pos + 2] * 65536 + wav[pos + 3] * 16777216;
        pos += 4 + chunkSize;
    }
    pos += 8;

    // Pos is now positioned to start of actual sound data.
	int samples = (size )/2;     // 2 bytes per sample (16 bit sound mono)
    if (channels == 2) samples /= 2;        // 4 bytes per sample (16 bit stereo)

    // Allocate memory (right will be null if only mono sound)
    left = new double[samples];
    if (channels == 2) right = new double[samples];
    else right = NULL;

    // Write to short array/s:
    //int i=0;
    //while (pos < samples) {
    //    left[i] = bytesToDouble(wav[pos], wav[pos + 1]);
    //    pos += 2;
    //    if (channels == 2) {
    //        right[i] = bytesToDouble(wav[pos], wav[pos + 1]);
    //        pos += 2;
    //    }
    //    i++;
    //}

	for (int i = 0; i < samples; i++) {
		left[i] = bytesToDouble(wav[2*i], wav[2*i+1]);
	}

	return samples;
}

int main (int argc, char **argv)
{
	new Server(30);
	while(1){ Sleep(500000); }

	return(0);
}