#include "Lexical.h"
#include <fstream> 

Lexical::Lexical(string fileName)
	:fileName(fileName), lineNumber(1), elementCount(0), columnCount(0), groupNumber(0), prevElement(SPACE)
{
}

Lexical::Lexical()
	:fileName(""), lineNumber(1), elementCount(0), columnCount(0), groupNumber(0), prevElement(SPACE)
{

}

void Lexical::readBuffer(const char * buffer)
{
	char x = SPACE;
	stringstream stream;
	stream << buffer;

	while ( ( x = stream.get()) != EOF ) { 
		processElement(x);
	}
}

void Lexical::readFile()
{
	char x = SPACE;
    ifstream inFile(fileName);

    if (!inFile) {
        cout << "Unable to open file";
        exit(1);
    }
    
	while ( ( x = inFile.get()) != EOF ) { 
		processElement(x);
	}

    inFile.close();
}

bool Lexical::isPuntuation(char element)
{
	for(int i = 0; i < sizeof(puntuations); i++)
		if(element == puntuations[i]) 
			return true;

	return false;
}

bool Lexical::isWord(char element)
{
	//Capital A-Z
	if( element >= 'A' && element <= 'Z' ) return true;
	//Common a-z
	if( element >= 'a' && element <= 'z' ) return true;

	return false;
}

/**
* isLanguage
* determine if an element from file is langauge defined
* Params char
**/
bool Lexical::isLanguage(char element)
{
	if( element == START || element == STOP ) 
		return true;
	return false;
}

size_t Lexical::refSum()
{
	return elementCount * sizeof(char);
}

EnumElementType Lexical::getElementType(char element)
{
	if( isPuntuation(element) ) return ElementType::_PUNTUATION;
	if( isLanguage(element) ) return ElementType::_LANGUAGE;
	if( isWord(element) ) return ElementType::_WORD;
	return ElementType::_UNKNOWN;
}

void Lexical::processElement(char element)
{
	if( element != LINEFEED)
	{
		elementCount++;
		columnCount++;
	
		if( element != SPACE )
		{
			if( prevElement != SPACE )
				if( isPuntuation(element)  && !isPuntuation(prevElement) && element != APOS )//increment once before saving element
				{
					groupNumber++; //increment once before saving element
				}

			symbolTable.addSymbolReference(element, getElementType(element), 
				lineNumber, columnCount, groupNumber, refSum());

			if( isPuntuation(element) && element != APOS ) 
			{
				groupNumber++;
			}
		}
		else
		{
			if( prevElement != SPACE )
			if(!isPuntuation(prevElement) && element != APOS)
				groupNumber++;
		}

		if(	element == NEWLINE )
		{
			lineNumber++;
			columnCount = 0;
		}

		prevElement = element;
	}
}

