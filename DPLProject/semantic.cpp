#include "semantic.h"

//Checks for all Semantic features within the song file
void semantic::checkSemantics(SymbolTablePtr sTable)
{
	checkAll(sTable);
	checkWordsBetweenBrackets(sTable);
	checkWordsBetweenCurlyBrackets(sTable);
	checkForSongContent(sTable);
}

//assume both open and close to be the same amount
//from the syntax phase check
void semantic::checkLabel(SymbolTablePtr sTable, char open, char close, static bool callBack(SymbolReferenceGroupPtr), const CHARPTR errorMessage )
{
	MapValue openBracket, closeBracket;
	try
	{
		openBracket = sTable->getMapValue(open);
		closeBracket = sTable->getMapValue(close);

	}
	catch( ... )
	{
		//throw nothing if missing
		return;
	}

	//previous group
	SymbolReferenceGroupPtr prevGroup = NULL;

	//both open and close are side by side
	for(int i = 0; i < openBracket.refs->size(); i++)
	{
		SymbolReferencePtr refOpen = openBracket.refs->at(i);
		SymbolReferencePtr refClose = closeBracket.refs->at(i);
		SymbolReferenceGroupPtr group = sTable->getSymbolReferenceGroup(refOpen->getGroupNumber() + 1);

		//flag open and close as Language
		refOpen->setType(ElementType::_LANGUAGE);
		refClose->setType(ElementType::_LANGUAGE);
		
		if(group->size() == 1 && group->at(0)->getValue() == BACKSLASH)
		{
			group->at(0)->setType(ElementType::_LANGUAGE);
			group = sTable->getSymbolReferenceGroup(refOpen->getGroupNumber() + 2);
			//group here should equal the previous group
			if(prevGroup != NULL && !matchLabelGroup(group, prevGroup) )
			{
				throw ExceptionHandler("Label miss match ", group); 
			}
		}

		//flag the words as apart of language
		for(int e = 0; e < group->size(); e++)
		{
			group->at(e)->setType(ElementType::_LANGUAGE);
		}

		if( ! callBack(group) ) {
			throw ExceptionHandler(errorMessage, group); 
		}

		prevGroup = group;
		//if group complete set previous to null
		if(i % 2 == 0 ) prevGroup == NULL;
	}
}

bool semantic::matchLabelGroup(SymbolReferenceGroupPtr ptrA,  SymbolReferenceGroupPtr ptrB)
{
	if(ptrA->size() != ptrB->size())
		return false;

	for(int i = 0; i < ptrA->size(); i++)
		if( ptrA->at(i)->getValue() != ptrB->at(i)->getValue())
			return false;

	return true;
}

//checks for the verse keyword within braces 
void semantic::checkAll(SymbolTablePtr sTable)
{
	checkLabel(sTable, OPENSQBRACKET, CLOSESQBRACKET, checkAllLabels);
}

bool semantic::checkAllLabels(SymbolReferenceGroupPtr ptr)
{
	if(checkVerseCharacters(ptr) ||
		checkChorusCharacters(ptr) )
		return true;

	return false;
}

//checks for the individual characters of the verse keyword 
bool semantic::checkVerseCharacters(SymbolReferenceGroupPtr ptr)
{
	if( ptr->size() != 5)
		return false;

	if(	ptr->at(0)->getValue() == 'v' && ptr->at(1)->getValue() == 'e' &&
			ptr->at(2)->getValue() == 'r' && ptr->at(3)->getValue() == 's' && ptr->at(4)->getValue() == 'e')
	{
		return true;
	}

	return false;
}

//checks for the individual characters of the chorus keyword 
bool semantic::checkChorusCharacters(SymbolReferenceGroupPtr ptr)
{
	if( ptr->size() != 6)
		return false;

	if(	ptr->at(0)->getValue() == 'c' && ptr->at(1)->getValue() == 'h'&&
			ptr->at(2)->getValue() == 'o' && ptr->at(3)->getValue() == 'r' 
				&& ptr->at(4)->getValue() == 'u' && ptr->at(5)->getValue()=='s')
	{
		return true;
	}

	return false;
}

//checks for the actual word characters within brackets 
void semantic::checkWordsBetweenBrackets(SymbolTablePtr sTable)
{
	checkForSongContent(sTable, OPENBRACKET, CLOSEBRACKET);
}

//checks for the actual word characters within Curly brackets
void semantic::checkWordsBetweenCurlyBrackets(SymbolTablePtr sTable)
{
	checkForSongContent(sTable, OPENCRLYBRACKET, CLOSECRLYBRACKET);
}

void semantic::checkForSongContent(SymbolTablePtr sTable, char open, char close)
{
	MapValue openBracket, closeBracket;
	try
	{
		openBracket = sTable->getMapValue(open);
		closeBracket = sTable->getMapValue(close);

	}
	catch( ... )
	{
		//throw nothing if missing
		return;
	}

	//both open and close are side by side
	for(int i = 0; i < openBracket.refs->size(); i++)
	{
		SymbolReferencePtr refOpen = openBracket.refs->at(i);
		SymbolReferencePtr refClose = closeBracket.refs->at(i);
		SymbolReferenceGroupPtr between = sTable->getSymbolReferenceGroup(refOpen->getGroupNumber() + 1);

		if( ( between == NULL || between->size() == 0 )
			|| between->at(0)->getGroupNumber() == refClose->getGroupNumber() 
			)
			throw ExceptionHandler("Song content expected at ", refOpen );
	}
}

/*	checks for the actual song content within all tags
	such as verses and chorus content	*/
void semantic::checkForSongContent(SymbolTablePtr sTable)
{
	MapValue slash;
	try
	{
		slash = sTable->getMapValue(BACKSLASH);
	}
	catch( ... )
	{
		//throw nothing if missing
		return;
	}

	bool found = false;

	for(int i = 0; i < slash.refs->size(); i++)
	{
		SymbolReferenceGroupPtr words = sTable->getSymbolReferenceGroup(slash.refs->at(i)->getGroupNumber() - 2);
		SymbolReferenceGroupPtr bracket = sTable->getSymbolReferenceGroup(slash.refs->at(i)->getGroupNumber() - 1);

		if( words == NULL || words->size() == 0 )
			throw ExceptionHandler("Song content expected at ",  bracket);

		int e = slash.refs->at(i)->getGroupNumber();

		for(int e = slash.refs->at(i)->getGroupNumber(); e > 0; e-- )
		{
			SymbolReferenceGroupPtr tmp = sTable->getSymbolReferenceGroup(e);
			if( tmp != NULL && tmp->at(0)->getType() == ElementType::_WORD )
			{
				found = true;
				break;
			}
		}

		if( !found )
			throw ExceptionHandler("Song content expected at ",  bracket);
	}

}




