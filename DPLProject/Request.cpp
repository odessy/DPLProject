#include "Request.h"

//Default declaration of callback function
int RequestHandler::active = 0;

Request::Request( Socket * socket)
	: socket(socket)
{
}

Request::~Request()
{
	requestData.packets.clear();
	header.clear();
}

string Request::trim(string & str)
{
    size_t first = str.find_first_not_of(' ');
    size_t last = str.find_last_not_of(' ');
    return str.substr(first, (last-first+1));
}

void Request::processRequest()
{
    if( header["method"] == "GET" )
	{
		getRequest();
	}
	else if( header["method"] == "POST")
	{
		postRequest();
	}
	else
	{
		char buf[] = "HTTP/1.x 405 Method Not Allowed\r\nDate: Sun, 18 Aug 2013 16:50:06 GMT\r\nAllow: GET, POST\r\nConnection: close\r\nContent-Length: 0\r\n";
		socket->sendAll(buf, strlen(buf) , 0);
	}
}

void Request::copyCharToVector(vector<Packet> * vector, char * buf, size_t size)
{
	Packet packet = { new char[size], size};
	memcpy(packet.pack, buf, size);
	vector->push_back(packet);
}

void Request::readRequest()
{
	char	buffer[BUFFER_SIZE];
	int		read;

	//set the socket to nonblocking
	socket->setSocketBlockingEnabled(false);

	requestData.size = 0;
	requestData.packets = vector<Packet>(0);
	
	Sleep(100); //wait 100 ms before trying to read

	while( true )
	{
		memset(buffer, '\0', BUFFER_SIZE);
		read = socket->read(buffer, BUFFER_SIZE , 0);
		if( read > 0 && read <= BUFFER_SIZE)
		{
			copyCharToVector(&requestData.packets, buffer, read);
			requestData.size += read;
		}
		else 
			break;
	}
}

bool Request::parseHttpRequest()
{
	readRequest(); //read the request

	if( requestData.size == 0 ) return false;

	char * data = new char[requestData.size];
	char * content = NULL;
	int start = 0;

	for(int i = 0; i < requestData.packets.size(); i++)
	{
		memcpy(&data[start], requestData.packets.at(i).pack, requestData.packets.at(i).size);
		delete requestData.packets.at(i).pack;
		start += requestData.packets.at(i).size;
	}

	requestData.packets.clear();
	content = parseHttpHeader(data, requestData.size);

	//printf("%s", data);

	if(content != NULL)
	{
		int conlen = requestData.size - (content - data);
		if( conlen > 0)
			copyCharToVector(&requestData.packets, content, conlen);
		requestData.size = conlen;
	}

	delete data;

	return header.size() != 0;
}

char * Request::parseHttpHeader(char * buffer, size_t len)
{
	char    * eol; // end of line
	char    * bol; // beginning of line

	// process each line in buffer looking for header break
	bol = buffer;

	while((eol = strchr(bol, '\n')) != NULL)
	{
		//printf("\nparseHttpHeader() Looping\n");

		if( header.find("method") != header.end())
			processHeaderField(bol, eol);
		else
			processHeaderMethod(bol, eol);

		// update bol based upon the value of eol
		bol = eol + 1; 

		// test if end of headers has been reached
		if ( (!(strncmp(bol, "\r\n", 2))) || (!(strncmp(bol, "\n", 1))) )
		{
			// update the value of bol to reflect the beginning of the line
			// immediately after the headers
			if (bol[0] != '\n')
				bol += 1;

			//end of header
			bol += 1;
			break;
		}
	}

	return bol;
}

void Request::processHeaderField(char * bol, char * eol)
{
	parseHeaderField( getHeaderField(bol, eol) );
}

void Request::processHeaderMethod(char * bol, char * eol)
{
	parseHeaderMethod( getHeaderField(bol, eol) );
}

string Request::getHeaderField(char * bol, char * eol)
{
	string field;

	char * b = bol;

	while( b != eol + 1 )
	{
		//printf("\ngetHeaderField() Looping\n");
		field += *b;
		b++;
	}

	return field;
}

void Request::parseHeaderMethod(string field)
{
	string method;
	string url;
	string parts;

	size_t found = field.find(":");

	found  = field.find(" ");

	//printf("%s\n", field.c_str());

	if( found != -1)
	{
		method = field.substr( 0,  found );
		parts = field.substr( found+1, field.length() );
		url = parts.substr(0, parts.find(" "));

		if( method == "GET" 
			|| method == "POST" 
			|| method == "CONNECT"
			|| method == "HEAD"
			|| method == "DELETE"
			|| method == "PUT"
			|| method == "OPTIONS")
		{
			header["method"] = trim(method);
			header["url"] = trim(url);
		}
	}
}

void Request::parseHeaderField(string field)
{
	string key;
	string value;
	string clean;

	size_t found = field.find(":");

	//printf("%s", field.c_str());

	if( found != -1 )
	{
		key = field.substr( 0, found );
		value = field.substr( found+1, field.length() );
		clean = value.substr(0, value.find("\r"));
		header[key] = trim(clean);
	}
}

int Request::isurl(const char * url)
{
	int start = 7;

	if( strlen(url) < start ) return 0;

	if( ( url[0] == 'h' && url[1] =='t' && url[2] == 't' && url[3] == 'p' )
		&& ( url[4] == ':' || ( url[4] == 's' && (start += 1) ) )
		&& ( url[start-1] == '/' && url[start-2] == '/' ) )

		return start;

	return 0;
}

string Request::getHostFromUrl(string address)
{
	string hostpart = "";
	const char * url = address.c_str();
	int start = isurl(url), end = 0;

	if(address.empty()) return hostpart;

	while( end < strlen(url) )
	{
		if( url[start+end] != '/' )
			hostpart += url[start+end];
		else
			break;
		end++;
	}

	return hostpart;
}

Socket * Request::getSocket()
{
	return socket;
}

void Request::getRequest()
{
	if(header["url"].find('.') != -1)
	{
		string headerString = "HTTP/1.1 200 OK\r\nServer: dpl\r\nCache-Control: no-cache, no-store, must-revalidate\r\nPragma: no-cache\r\nExpires: 0\r\nContent-Type: audio/x-wav\r\n";
		sendFile(header["url"].substr(1, header["url"].length()) , headerString);
		socket->closeSocket();	
	}
	else
	{
		string headerString = "HTTP/1.1 200 OK\r\nServer: dpl\r\nContent-Type: text/html; charset=utf-8\r\n";
		sendFile("./html/index.html", headerString);
		socket->closeSocket();
	}
}

void Request::sendFile(string fileName, string header, const char * replace)
{
	ifstream inFile(fileName, ios::binary);

	if (!inFile.is_open()) {
        cout << "Unable to open file " << fileName << endl;
		string headerString = "HTTP/1.1 404 notfound\r\nServer: dpl\r\nContent-Type: text/html; charset=utf-8\r\n\r\n";
		socket->sendAll((char *)headerString.c_str(), headerString.size(), 0);
        return; 
	}

    inFile.seekg (0, inFile.end);
    int size = inFile.tellg();
    inFile.seekg (0, inFile.beg);
    char * buffer = new char [size];
	char * lbuffer = NULL;
    inFile.read (buffer,size);

	
	if( replace != NULL )
	{
		int i = 1;
		int offset = 0;
		while(i < size)
		{
			if( buffer[i-1] == '%' && buffer[i] == 's')
			{
				offset = i - 1;

				lbuffer = new char[(size - 2) + strlen(replace)];
				memset(lbuffer, '\0', size + strlen(replace)); 
				if( lbuffer == NULL) return;

				//buffer up to offset
				memcpy(lbuffer, buffer, offset);
				//Add additional characters
				memcpy(lbuffer + offset, replace, strlen(replace) );
				//Add rest of the buffer
				memcpy(lbuffer + offset + strlen(replace) , buffer + i + 1, size - (i + 1)); 

				if( buffer != NULL) delete buffer;
				buffer = lbuffer;
				//set new size
				size = (size - 2) + strlen(replace);
				break;
			}
			i++;
		}
	}

	header+= "Connection: keep-alive\r\nContent-Length: " + to_string(size) + "\r\n\r\n";
	socket->sendAll((char *)header.c_str(), header.size(), 0);
	socket->sendAll(buffer, size, 0, 30);

	//clean up buffer
	//if( buffer != NULL) delete buffer;
}

void Request::postRequest()
{
	int loc = header["Content-Type"].find('=');
	string boundary = header["Content-Type"].substr(loc+1, header["Content-Type"].length());

	MultipartConsumer * con = new MultipartConsumer(("--"+boundary).c_str());

	if( con->CountHeaders(requestData.packets.at(0).pack) > 0)
	{
		if(con->headers.find("file") != con->headers.end())
		{
			try
			{
				start (con->headers["file"], "default", "testing.wav", 1);
				string headerString = "HTTP/1.1 200 OK\r\nServer: dpl\r\nContent-Type: text/html; charset=utf-8\r\n";
				sendFile("./html/play.html", headerString,  "/testing.wav");
				socket->closeSocket();
			}
			catch(ExceptionHandler& handle)
			{
				handleError(handle);
			}
		}
	}

	//string headerString = "HTTP/1.1 301 OK\r\nServer: nginx\r\nLocation: /testing.wav\r\n";
	//socket->sendAll((char *)headerString.c_str(), headerString.size(), 0);
}

void Request::handleError(ExceptionHandler& handle)
{
	string headerString = "HTTP/1.1 501 error\r\nServer: dpl\r\nContent-Type: text/html; charset=utf-8\r\n";
	sendFile("html/error.html", headerString, handle.what());
	socket->closeSocket();
}
