#ifndef REQUEST_H
#define REQUEST_H

#include "socket.h"
#include "Thread.h"
#include "MultipartConsumer.h"
#include "Espeak.h"
#include "Lexical.h"
#include "ExceptionHandler.h"

#include <map>
#include <vector>
#include <string>
#include <algorithm>
#include <fstream>

typedef struct packet{ char * pack; size_t size; } Packet;

#define CHK_NULL(x) if ((x)==NULL) exit (1)
#define CHK_ERR(err,s) if ((err)==-1) { perror(s); exit(1); }
#define CHK_SSL(err) if ((err)==-1) { ERR_print_errors_fp(stderr); exit(2); }

typedef struct function { Socket * socket; string header; string url; } Callback;
struct data { Socket * socket; char trace_ascii; /* 1 or 0 */  };
typedef struct requestData { size_t size; vector<Packet> packets; } RequestData; 

class Request
{

private:
	Callback responseObj;
	Socket * socket;

public:

	map<string, string>	header;
	RequestData requestData;

	Request(Socket * socket);
	Request(Socket * socket, string host, string port);
	~Request();
	
	static string trim(string & str);

	void processRequest();
	void readRequest();
	bool parseHttpRequest() throw();
	void processHeaderField(char * bol, char * eol);
	void processHeaderMethod(char * bol, char * eol);
	string getHeaderField(char * bol, char * eol);
	void parseHeaderField(string field);
	void parseHeaderMethod(string field);
	char * parseHttpHeader(char * buffer, size_t len);

	void addRequestHeaders();

	Socket * getSocket();
	string getHostFromUrl(string address);
	int isurl(const char * url);
	void getRequest();
	void postRequest();
	void sendFile(string name, string header, const char * replace = NULL);

	static void copyCharToVector(vector<Packet> * vector, char * buf, size_t size);
	void handleError(ExceptionHandler& handler);

};


//Request Handler Class

class RequestHandler : public Thread::ActiveObject
{
public:
	static int active;
	void Start(){ _thread.Resume(); }
	void Stop(){ _thread.Suspend(); }
	RequestHandler(Socket * socket )
		:socket(socket)
	{ Start(); }

	~RequestHandler(){
		Kill(); 
	}
private:
	Socket * socket;
	void InitThread () {}

	void httpRequest()
	{
		try
		{
			Request * request = new Request(socket);
			//Check if it was for closing , and also read the incoming message
			
			
			if( request->parseHttpRequest() )
			{
				request->processRequest();
			}

			active--;
			if(!socket->isLocked())
			{
				socket->closeSocket();
			}
			//delete socket;
			//clean up
			delete request;
			delete this;
		}
		catch(...)
		{
			printf("\nFatal RequestHandler Exception Occured. Exiting progaram\n");
			system("pause");
			exit(0);
		}
	}

	void Loop()
	{
		httpRequest();
	}

	void FlushThread() {}

};

#endif