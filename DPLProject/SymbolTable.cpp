#include "SymbolTable.h"

SymbolTable::SymbolTable()
	:symbolTableVector(SymbolTableVector(0)), valueMap(ValueMap())
{

}

SymbolTable::~SymbolTable()
{
	//clear all vector values
	for(int i = 0; i < symbolTableVector.size(); i++)
	{
		SymbolReferenceGroupPtr ptr = symbolTableVector.at(i);
		ptr->clear();
		delete ptr;
	}
	valueMap.clear();
	symbolTableVector.clear();
}

void SymbolTable::addSymbolReference(char value, EnumElementType type, 
		int lineNumber, int columnNumber, int groupNumber, size_t seekRef)
{
	CHARPTR valueRef = NULL;
	ValueMap::iterator i = valueMap.find(value);
	MapValue mapValue;
	if( i == valueMap.end())
	{
		mapValue.charPtr = new char(value);
		mapValue.refs = new SymbolReferenceGroup(0);

		valueMap[value] = mapValue;
		valueRef = mapValue.charPtr;
	}
	else
	{
		mapValue = ((MapValue)i->second);
		valueRef = mapValue.charPtr;
	}

	SymbolReferencePtr symbolReference = new SymbolReference(valueRef, type, lineNumber, columnNumber, groupNumber, seekRef);
	//Add reference to map also
	mapValue.refs->push_back(symbolReference);
	//addreference group also
	addToReferenceGroup(symbolReference);
}

void SymbolTable::addToReferenceGroup(SymbolReferencePtr symbolReference)
{
	if( symbolTableVector.size() > symbolReference->getGroupNumber() )
	{
		SymbolReferenceGroupPtr ptr = symbolTableVector.at(symbolReference->getGroupNumber());
		ptr->push_back(symbolReference);
	}
	else
	{
		SymbolReferenceGroupPtr group = new SymbolReferenceGroup(0);
		group->push_back(symbolReference);
		symbolTableVector.push_back(group);
	}
}

SymbolReferenceGroupPtr SymbolTable::getSymbolReferenceGroup(int index)
{
	if( symbolTableVector.size() > index)
	{
		return symbolTableVector.at(index);
	}

	return NULL;
}

int SymbolTable::getSize()
{
	return symbolTableVector.size();
}

void SymbolTable::displayValueMap()
{
	for(ValueMap::const_iterator iter = valueMap.begin(); iter != valueMap.end(); iter++)
	{
		cout << iter->first << endl;
	}
}

const MapValue SymbolTable::getMapValue(char ch)
{
	ValueMap::iterator i = valueMap.find(ch);
	MapValue mapValue;

	if( i != valueMap.end())
		mapValue = ((MapValue)i->second);
	else
		throw("notfound");

	return mapValue;
}

void SymbolTable::displaySymbolTable()
{
	for(int e = 0; e < getSize(); e++)
	{
		SymbolReferenceGroupPtr group = getSymbolReferenceGroup(e);
		if( group != NULL )
		{
			for(int i = 0; i < group->size(); i++)
			{
				group->at(i)->display();
			}
		}
	}
}