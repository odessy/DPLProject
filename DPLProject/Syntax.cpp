#include "Syntax.h"

//checks for indicators and denotes abscence
void Syntax::checkPuntuations(SymbolTablePtr sTable)
{
	checkStart(sTable);
	checkStop(sTable);
	checkCurveBracket(sTable);
	checkCrlyBracket(sTable);
	checkQBracket(sTable);
}

//checks for start denoter [completed]
void Syntax::checkStart(SymbolTablePtr sTable)
{

	MapValue mapValue;

	try
	{
		mapValue = sTable->getMapValue(START);
	}
	catch(...)
	{
		throw ExceptionHandler("Missing Start Indicator");
	}	

	for(int i=0; i < mapValue.refs->at(0)->getGroupNumber(); i++)
	{
		SymbolReferenceGroupPtr group = sTable->getSymbolReferenceGroup(i);			
		if(group->at(0)->getValue() != NEWLINE)
		{
			throw ExceptionHandler("Characters Before Start Indicator", group->at(i));
		}
	}
}

//checks for stop denoter
void Syntax::checkStop(SymbolTablePtr sTable)
{
	MapValue mapValue;

	try
	{
		mapValue = sTable->getMapValue(STOP);
	}
	catch( ... )
	{
		throw ExceptionHandler("Missing Stop Indicator");
	}

	for(int i=mapValue.refs->at(0)->getGroupNumber()+1; i < sTable->getSize(); i++)
	{
		SymbolReferenceGroupPtr group = sTable->getSymbolReferenceGroup(i);
		if(group->at(0)->getValue() != NEWLINE)
		{
			throw ExceptionHandler("Characters After End Indicator", group->at(0));	
		}
	}
}

void Syntax::checkBracket(SymbolTablePtr sTable, char open, char close, const CHARPTR errorMessage)
{

	int openBrace=0;
	int closeBrace=0;
	int value=0;
	int notifier=0;	
	SymbolReferenceGroupPtr group = NULL;
	
	for(int i=0; i < sTable->getSize(); i++)
	{		
		group = sTable->getSymbolReferenceGroup(i);

		if(group->at(0)->getType() == _PUNTUATION)
		{		
			if(value > 1)
				break;

			if(group->at(0)->getValue() == open)
			{
				openBrace++;
				value++;
			}
			else if(group->at(0)->getValue() == close)
			{
				closeBrace++;
				value=0;
			}
			
		}
		
	}
	
	if(openBrace!=closeBrace || value != 0)
		throw ExceptionHandler(errorMessage, group->at(0) );	

}

//checks for open and close braces
void Syntax::checkCurveBracket(SymbolTablePtr sTable)
{	
	checkBracket(sTable, OPENBRACKET, CLOSEBRACKET, "Missing Curve Bracket");
}

//check for Curly bracket
void Syntax::checkCrlyBracket(SymbolTablePtr sTable)
{
	checkBracket(sTable, OPENCRLYBRACKET, CLOSECRLYBRACKET, "Missing Curly Bracket");
}

//check for square brackets
void Syntax::checkQBracket(SymbolTablePtr sTable)
{
	checkBracket(sTable, OPENSQBRACKET, CLOSESQBRACKET, "Missing Square Bracket");
}
